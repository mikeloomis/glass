//
// Created by Mike Loomis on 3/25/2020.
//

#include "gui/mainwindow.h"
#include "app.hpp"

#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication qapp(argc, argv);
  Q_INIT_RESOURCE(resources);
  auto app = glass::App{};

  MainWindow main_win{&app};
  main_win.load_from_cache();
  main_win.show();

  QObject::connect(&main_win, &MainWindow::opened_project,
                   [&](auto root_path) { app.useFolderAsProject(root_path); });

  return QApplication::exec();
}
