//
// Created by Mike Loomis on 4/18/2020.
//

#ifndef GLASS_APP_HPP
#define GLASS_APP_HPP

#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include "project.hpp"

namespace glass {

using json = nlohmann::json;

struct SettingCache {
 public:
  fs::path file_path;
  nlohmann::json settings;

  explicit SettingCache(const fs::path &path)
      : file_path{fs::absolute(path)} {
    settings = fetchSettings(file_path);
  }

  void fetch();
  void commit() const;

  template<class Value>
  inline void update_setting(const std::string &key, const Value &val) {
    fetch();
    settings[key] = val;
    commit();
  }

  inline void append_settings(const nlohmann::json &new_vals) {
    fetch();
    try {
      settings.update(new_vals);
    } catch (const std::exception &e) {
      spdlog::error(e.what());
    }
    commit();
  }
};

class App {
 public:
  static constexpr auto app_name = R"(glass)";
  static constexpr auto config_filename = R"(configs.json)";
  static constexpr auto cache_filename = R"(cache.json)";

  App();
  explicit App(Directories non_default_dirs); // Mainly for mocking

  // Creates a glass dir in a folder with project settings
  // Also caches the project folder for later use
  void useFolderAsProject(const fs::path &root_dir);

  void addConfigs(const json &new_configs) {
    user_config.append_settings(new_configs);
  }

  void addCacheData(const json &new_cache) {
    user_cache.append_settings(new_cache);
  }

  template<class T>
  struct get_result {
    bool contains;
    T value;
  };

  template<class T>
  auto getConfig(const std::string &key) {
    return get_result<T>{user_config.settings.contains(key), user_config.settings.value(key, T{})};
  }

  template<class T>
  auto getCache(const std::string &key) {
    return get_result<T>{user_cache.settings.contains(key), user_cache.settings.value(key, T{})};
  }

 private:
  Directories dirs;

  SettingCache user_config;
  SettingCache user_cache;
};
} // namespace glass



#endif //GLASS_APP_HPP
