#include "mainwindow.h"

#include <forms/ui_mainwindow.h>
#include <QtWidgets/QFileSystemModel>
#include <QtWidgets/QFileDialog>

#include <spdlog/spdlog.h>

MainWindow::MainWindow(glass::App *parent_app, QWidget *parent)
    : QMainWindow{parent},
      ui{new Ui::MainWindow},
      app{parent_app} {
  ui->setupUi(this);

  layout = new QHBoxLayout(ui->centralwidget);
  ui->centralwidget->setLayout(layout);
  centralPanel = new CentralPanel{};
  closedPlaceholder = new ClosedPlaceholder{};
  layout->addWidget(centralPanel);
  layout->addWidget(closedPlaceholder);
  centralPanel->hide();

  connect(ui->actionNew, &QAction::triggered,
          []() { spdlog::info("New project not yet implemented..."); });

  connect(ui->actionOpen, &QAction::triggered,
          [this]() {
            QFileDialog dialog{this};
            dialog.setFileMode(QFileDialog::FileMode::Directory);
            dialog.setViewMode(QFileDialog::ViewMode::List);
            dialog.setAcceptMode(QFileDialog::AcceptMode::AcceptOpen);
            if (dialog.exec() == 0) {
              spdlog::error("Open folder dialog failed in some way.");
              return;
            }

            auto path = glass::fs::path(dialog.selectedFiles().front().toStdString());
            spdlog::info("Selected: {}", path.string());

            open_project(path);
          });
  connect(QCoreApplication::instance(), &QCoreApplication::aboutToQuit,
          this, &MainWindow::cache_window_settings);

  connect(ui->actionClose_Project, &QAction::triggered,
          [this]() {
            close_project();
          });
}

MainWindow::~MainWindow() {
  delete ui;
  delete centralPanel;
  delete closedPlaceholder;
}

void MainWindow::load_from_cache() {
  auto project_root_path = app->getCache<std::string>("last_project_root");
  if (project_root_path.contains && !project_root_path.value.empty()) {
    open_project({project_root_path.value});
  }

  auto maximized = app->getCache<bool>("maximized");
  if (maximized.contains && maximized.value) {
    showMaximized();
  } else {
    auto width = app->getCache<int>("window_width");
    auto height = app->getCache<int>("window_height");
    auto window_x = app->getCache<int>("window_x");
    auto window_y = app->getCache<int>("window_y");

    if (width.contains && height.contains && window_x.contains && window_y.contains) {
      setGeometry(window_x.value, window_y.value, width.value, height.value);
    }
  }
}

void MainWindow::cache_window_settings() {
  app->addCacheData({{"maximized", isMaximized()}});
  app->addCacheData(
      {
          {"window_width", width()},
          {"window_height", height()}
      });
  app->addCacheData(
      {
          {"window_x", geometry().x()},
          {"window_y", geometry().y()}
      });
}

void MainWindow::open_project(const glass::fs::path &root) {
  emit opened_project(root);
  centralPanel->set_project_root(root);
  closedPlaceholder->hide();
  centralPanel->show();
}

void MainWindow::close_project() {
  centralPanel->hide();
  closedPlaceholder->show();
}