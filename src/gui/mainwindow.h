#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QVBoxLayout>

#include "../app.hpp"
#include "closedplaceholder.h"
#include "centralpanel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
 Q_OBJECT

 public:
  explicit MainWindow(glass::App *parent_app, QWidget *parent = nullptr);
  MainWindow(const MainWindow &other) = delete;
  MainWindow(MainWindow &&other) noexcept = delete;
  MainWindow &operator=(const MainWindow &other) = delete;
  MainWindow &operator=(MainWindow &&other) noexcept = delete;
  ~MainWindow() override;

  void load_from_cache();
  void cache_window_settings();

 signals:
  void opened_project(const glass::fs::path &root);

 private:
  void open_project(const glass::fs::path &root);
  void close_project();

  Ui::MainWindow *ui;
  QHBoxLayout *layout;
  ClosedPlaceholder *closedPlaceholder;
  CentralPanel *centralPanel;

  glass::App *app;
};

#endif // MAINWINDOW_H
