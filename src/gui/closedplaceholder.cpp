#include "closedplaceholder.h"
#include "forms/ui_closedplaceholder.h"

ClosedPlaceholder::ClosedPlaceholder(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClosedPlaceholder) {
  ui->setupUi(this);
}

ClosedPlaceholder::~ClosedPlaceholder() {
  delete ui;
}
