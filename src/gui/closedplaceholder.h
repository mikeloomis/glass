#ifndef CLOSEDPLACEHOLDER_H
#define CLOSEDPLACEHOLDER_H

#include <QWidget>

namespace Ui {
class ClosedPlaceholder;
}

class ClosedPlaceholder : public QWidget {
 Q_OBJECT

 public:
  explicit ClosedPlaceholder(QWidget *parent = nullptr);
  ~ClosedPlaceholder();

 private:
  Ui::ClosedPlaceholder *ui;
};

#endif // CLOSEDPLACEHOLDER_H
