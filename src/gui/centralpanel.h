#ifndef CENTRALPANEL_H
#define CENTRALPANEL_H

#include <QWidget>
#include <QtWidgets/QFileSystemModel>
#include <QtCore/QSortFilterProxyModel>

#include "../project.hpp"

namespace Ui {
class CentralPanel;
}

class CentralPanel : public QWidget {
 Q_OBJECT

 public:
  explicit CentralPanel(QWidget *parent = nullptr);
  ~CentralPanel();

  void set_project_root(const glass::fs::path &root);

 private:
  Ui::CentralPanel *ui;

  QSortFilterProxyModel proxy_fs_model{};
  QFileSystemModel fs_model{};
};

#endif // CENTRALPANEL}_H
