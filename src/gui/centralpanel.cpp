#include "centralpanel.h"
#include "forms/ui_centralpanel.h"

CentralPanel::CentralPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CentralPanel) {
  ui->setupUi(this);

  proxy_fs_model.setSourceModel(&fs_model);
  ui->project_treeview->setModel(&proxy_fs_model);
  ui->project_treeview->hideColumn(1);
  ui->project_treeview->hideColumn(2);
  ui->project_treeview->hideColumn(3);
}

CentralPanel::~CentralPanel() {
  delete ui;
}

void CentralPanel::set_project_root(const glass::fs::path &root) {
  fs_model.setRootPath(root.string().c_str());
  ui->project_treeview->setModel(&fs_model);
  ui->project_treeview->setRootIndex(fs_model.index(root.string().c_str()));
}
