//
// Created by Mike Loomis on 4/5/2020.
//

#include "project.hpp"

#include <algorithm>
#include <vector>
#include <fstream>
#include <iomanip>

#include <spdlog/spdlog.h>

glass::createFolder_result glass::createFolder(const std::string &name, const fs::path &root) {

  if (!fs::exists(root)) {
    return {false, createFolder_result::Error::invalid_root};
  }

  if (fs::exists(root / name)) {
    return {false, createFolder_result::Error::folder_already_exists};
  }
  auto result = fs::create_directory(root / name);

  if (!result) {
    spdlog::error("Unknown filesystem error not handled while creating: {}", (root / name).string());
    return {result, createFolder_result::Error::filesystem_error};
  }
  return {result, createFolder_result::Error::none};
}

glass::createFolder_result cf_helper(const glass::fs::path &dir) {
  if (!glass::fs::exists(dir.parent_path())) {
    cf_helper(dir.parent_path());
  }
  return glass::createFolder(dir.filename().string(), dir.parent_path());
}

auto shared_constructor_code = [](glass::Directories &ad, const std::string &app_name) {
  if (app_name.empty()) {
    return; // Don't create folders in non-app space
  }

  auto results = std::vector<glass::createFolder_result>{
      cf_helper(ad.data_dir),
      cf_helper(ad.config_dir),
      cf_helper(ad.cache_dir),
      cf_helper(ad.documents),
      cf_helper(ad.desktop),
      cf_helper(ad.pictures),
      cf_helper(ad.music),
      cf_helper(ad.video),
      cf_helper(ad.download)
  };
  auto success_or_already_exists = [](const auto &result) {
    return !result.success && result.error != glass::createFolder_result::Error::folder_already_exists;
  };
  if (std::any_of(results.begin(), results.end(), success_or_already_exists)) {
    ad.removeAll();
  }
};

#ifdef _WIN32
#include <Shlobj.h>
#include <locale>
#include <codecvt>

namespace glass {
auto get_path = [](const auto &app_name, const auto &rfid) {
  PWSTR path{};
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
  SHGetKnownFolderPath(rfid, KNOWN_FOLDER_FLAG::KF_FLAG_DEFAULT, nullptr, &path);
  auto narrowed_path = converter.to_bytes(path);
  CoTaskMemFree(path);
  return fs::absolute(fs::path{narrowed_path} / app_name);
};
} // namespace

glass::Directories::Directories(const std::string &app_name)
    : app_name(app_name),
      data_dir(get_path(app_name, FOLDERID_LocalAppData) / "data"),
      config_dir(get_path(app_name, FOLDERID_LocalAppData) / "config"),
      cache_dir(get_path(app_name, FOLDERID_LocalAppData) / "cache"),
      documents(get_path(app_name, FOLDERID_Documents)),
      desktop(get_path(app_name, FOLDERID_Desktop)),
      pictures(get_path(app_name, FOLDERID_Pictures)),
      music(get_path(app_name, FOLDERID_Music)),
      video(get_path(app_name, FOLDERID_Videos)),
      download(get_path(app_name, FOLDERID_Downloads)) {
  shared_constructor_code(*this, app_name);
}

#elif __linux__
#include <cstdlib>
#include <string>

auto get_abs_home_path = [](){
  char *env_home = std::getenv("HOME");
  if(env_home != nullptr) {
    return glass::fs::absolute(env_home);
  } else {
    return glass::fs::absolute("~/");
  }
};
glass::Directories::Directories(const std::string &name)
    : app_name(name),
      data_dir(get_abs_home_path() / ".local" / "share" / app_name),
      config_dir(get_abs_home_path() / ".config" / app_name),
      cache_dir(get_abs_home_path() / ".cache" / app_name),
      documents(get_abs_home_path() / app_name),
      desktop(get_abs_home_path() / "Desktop" / app_name),
      pictures(get_abs_home_path() / "Pictures" / app_name),
      music(get_abs_home_path() / "Music" / app_name),
      video(get_abs_home_path() / "Video" / app_name),
      download(get_abs_home_path() / "Downloads" / app_name) {
  shared_constructor_code(*this, app_name);
}
#elif __APPLE__
#include <cstdlib>
#include <string>

auto get_abs_home_path = [](){
  char *env_home = std::getenv("HOME");
  if(env_home != nullptr) {
    return glass::fs::absolute(env_home);
  } else {
    return glass::fs::absolute("~/");
  }
};
glass::Directories::Directories(const std::string &name)
    : app_name(name),
      data_dir(get_abs_home_path() / "Library" / "Application Support" / app_name),
      config_dir(get_abs_home_path() / "Library" / "Preferences" / app_name),
      cache_dir(get_abs_home_path() / "Library" / "Caches" / app_name),
      documents(get_abs_home_path() / app_name),
      desktop(get_abs_home_path() / "Desktop" / app_name),
      pictures(get_abs_home_path() / "Pictures" / app_name),
      music(get_abs_home_path() / "Music" / app_name),
      video(get_abs_home_path() / "Video" / app_name),
      download(get_abs_home_path() / "Downloads" / app_name) {
  shared_constructor_code(*this, app_name);
}
#else
static_assert(false, "Unknown OS not supported");
#endif

void glass::Directories::removeAll() {
  fs::remove_all(data_dir);
  fs::remove_all(config_dir);
  fs::remove_all(cache_dir);
  auto remove_if_app_folder_exists = [this](const auto &dir) {
    if (dir.filename() == app_name) {
      fs::remove_all(dir);
    }
  };
  remove_if_app_folder_exists(data_dir.parent_path());
  remove_if_app_folder_exists(documents);
  remove_if_app_folder_exists(desktop);
  remove_if_app_folder_exists(pictures);
  remove_if_app_folder_exists(music);
  remove_if_app_folder_exists(video);
  remove_if_app_folder_exists(download);
}

glass::Directories::Directories(std::string name, const fs::path &only_local_root)
    : app_name(std::move(name)),
      data_dir(only_local_root / app_name / "data"),
      config_dir(only_local_root / app_name / "config"),
      cache_dir(only_local_root / app_name / "cache"),
      documents(only_local_root / app_name / "documents"),
      desktop(only_local_root / app_name / "desktop"),
      pictures(only_local_root / app_name / "pictures"),
      music(only_local_root / app_name / "music"),
      video(only_local_root / app_name / "video"),
      download(only_local_root / app_name / "download") {
  shared_constructor_code(*this, app_name);
}

std::ostream &operator<<(std::ostream &os, const glass::Directories &ad) {
  os << ad.data_dir << '\n';
  os << ad.config_dir << '\n';
  os << ad.cache_dir << '\n';
  os << ad.documents << '\n';
  os << ad.desktop << '\n';
  os << ad.pictures << '\n';
  os << ad.music << '\n';
  os << ad.video << '\n';
  os << ad.download << '\n';
  return os;
}

glass::createFile_result glass::createSettingsFile(const fs::path &file_path, const glass::json &object) {
  if (fs::is_directory(file_path)) {
    return createFile_result{false, createFile_result::Error::invalid_path};
  }
  std::ofstream file(fs::absolute(file_path));
  file << std::setw(4) << object;
  file.close();
  return glass::createFile_result{true, createFile_result::Error::none};
}

glass::settings_result glass::settingsAsJson(const fs::path &file_path) {
  if (!fs::exists(file_path)) {
    return {false, settings_result::Error::no_settings_file, json::object()};
  }
  if (fs::is_directory(file_path) || !fs::is_regular_file(file_path)) {
    return {false, settings_result::Error::invalid_path, json::object()};
  }
  std::ifstream file(file_path);
  return {true, settings_result::Error::none, json::parse(file)};
}

nlohmann::json glass::fetchSettings(const fs::path &file_path) {
  auto settings_res = settingsAsJson(file_path);
  if (settings_res.success) {
    spdlog::info("Fetching settings was successful: {}", file_path.string());
    return settings_res.json_object;
  }

  if (settings_res.error == settings_result::Error::no_settings_file) {
    auto create_file = createSettingsFile(file_path, settings_res.json_object);
    if (create_file.success) {
      spdlog::info("Initialized new settings file: {}", file_path.string());
    } else if (create_file.error == createFile_result::Error::invalid_path) {
      spdlog::error("Invalid path used for settings file: {}", file_path.string());
    } else {
      spdlog::error("Unknown error when creating settings file: {}", file_path.string());
    }
  } else if (settings_res.error == settings_result::Error::invalid_path) {
    spdlog::error("Invalid path used to fetch settings: {}", file_path.string());
  }

  return nlohmann::json::object();
}
