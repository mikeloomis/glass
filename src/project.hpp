//
// Created by Mike Loomis on 4/5/2020.
//

#ifndef PROJECT_HPP
#define PROJECT_HPP

#include <ostream>

#include <nlohmann/json.hpp>

#if __has_include(<filesystem>)
#include <filesystem>
#elif __has_include(<experimental/filesystem>)
#include <experimental/filesystem>
#else
#error "Missing <filsystem>"
#endif

namespace glass {

#if __has_include(<filesystem>)
#include <filesystem>
namespace fs = std::filesystem;
#elif __has_include(<experimental/filesystem>)
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#error "Missing <filsystem>"
#endif

struct createFolder_result {
  enum struct Error {
    none,
    folder_already_exists,
    invalid_root,
    filesystem_error
  };
  bool success;
  Error error;
};
createFolder_result createFolder(const std::string &folder, const fs::path &root);

struct Directories {
 public:
  enum class DirTypes {
    data,
    config,
    cache
  };

  std::string app_name;
  fs::path data_dir;
  fs::path config_dir;
  fs::path cache_dir;

  fs::path documents;
  fs::path desktop;
  fs::path pictures;
  fs::path music;
  fs::path video;
  fs::path download;

  explicit Directories(const std::string &app_name);

  Directories(std::string app_name, const fs::path &only_local_root);

  /*
   * Uses the filesystem::removeAll(path) function on each app directory.
   * Does not throw, can call multiple times.
   */
  void removeAll();
};

std::ostream &operator<<(std::ostream &os, const glass::Directories &ad);

using json = nlohmann::json;

struct createFile_result {
  enum class Error {
    none,
    invalid_path
  };
  bool success;
  Error error;
};

createFile_result createSettingsFile(const fs::path &file_path, const json &object);

struct settings_result {
  enum class Error {
    none,
    invalid_path,
    no_settings_file
  };
  bool success;
  Error error;
  json json_object;
};

settings_result settingsAsJson(const fs::path &file_path);

/*
 * Tries to get settings at file_path. If settings file doesn't exist,
 * creates an empty json settings object and returns it.
 */
nlohmann::json fetchSettings(const fs::path &file_path);
} // namespace glass

#endif //PROJECT_HPP
