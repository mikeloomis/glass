//
// Created by Mike Loomis on 4/18/2020.
//

#include <cassert>
#include <fstream>
#include <utility>

#include <spdlog/spdlog.h>

#include "app.hpp"

glass::App::App()
    : dirs{app_name},
      user_config{dirs.config_dir / config_filename},
      user_cache{dirs.cache_dir / cache_filename} {
}

glass::App::App(glass::Directories non_default_dirs)
    : dirs(std::move(non_default_dirs)),
      user_config{dirs.config_dir / config_filename},
      user_cache{dirs.cache_dir / cache_filename} {
}

void glass::App::useFolderAsProject(const fs::path &root_dir) {
  auto result = createFolder(fmt::format(".{}", app_name), root_dir);
  if (!result.success) {
    spdlog::error("Could not create project folder at: {}", root_dir.string());
  }

  user_cache.update_setting("last_project_root", root_dir.string());
}

void glass::SettingCache::commit() const {
  auto result = createSettingsFile(file_path, settings);
  if (!result.success) {
    spdlog::error("Could not commit settings to disk with path: {}", file_path.string());
  }
}

void glass::SettingCache::fetch() {
  settings = fetchSettings(file_path);
}
