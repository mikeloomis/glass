#define CATCH_CONFIG_RUNNER

#include <catch2/catch.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/null_sink.h>

int main(int argc, char *argv[]) {
  int result{};
  auto logger = spdlog::create<spdlog::sinks::null_sink_st>("null_logger");
  spdlog::set_default_logger(logger);
  try {
    result = Catch::Session().run(argc, argv);
  } catch (const std::exception &e) {
    UNSCOPED_INFO("Exception in tests");
  }

  return result;
}
