//
// Created by Mike Loomis on 4/5/2020.
//

#include <catch2/catch.hpp>

#include <fstream>
#include <iomanip>

#include "../src/project.hpp"

TEST_CASE("Can create folders at a given directory", "[createFolder]") {
  using namespace glass;
  constexpr auto test_folder_name = R"(test_folder)";
  auto abs_path = fs::current_path() / test_folder_name;
  if (fs::exists(abs_path)) {
    fs::remove(abs_path);
  }
  REQUIRE(!fs::exists(abs_path));
  auto result = createFolder(test_folder_name, fs::current_path());
  REQUIRE(result.success);
  REQUIRE(result.error == createFolder_result::Error::none);
  REQUIRE(fs::exists(abs_path));
  fs::remove(abs_path);
  REQUIRE(!fs::exists(abs_path));
}

TEST_CASE("createFolder_results::Error::invalid_root", "[createFolder]") {
  constexpr auto folder = R"(nonexistent_folder)";
  REQUIRE(!glass::fs::exists(glass::fs::current_path() / folder));
  auto result = glass::createFolder("test", glass::fs::current_path() / folder);
  REQUIRE(!result.success);
  REQUIRE(result.error == glass::createFolder_result::Error::invalid_root);
  REQUIRE(!glass::fs::exists(glass::fs::current_path() / folder));
}

TEST_CASE("createFolder_results::Error::already_exists", "[createFolder]") {
  constexpr auto folder = R"(folder_already_exists)";
  glass::fs::remove_all(glass::fs::current_path() / folder);
  auto result = glass::createFolder(folder, glass::fs::current_path());
  REQUIRE(result.success);
  REQUIRE(result.error == glass::createFolder_result::Error::none);
  REQUIRE(glass::fs::exists(glass::fs::current_path() / folder));
  result = glass::createFolder(folder, glass::fs::current_path());
  REQUIRE(!result.success);
  REQUIRE(result.error == glass::createFolder_result::Error::folder_already_exists);
  glass::fs::remove_all(glass::fs::current_path() / folder);
}

TEST_CASE("App directories are created (and removed after test) for this platform", "[Directories]") {
  auto ad = glass::Directories{""};
  REQUIRE(ad.app_name.empty());
  ad = glass::Directories("test");
  REQUIRE(!ad.app_name.empty());
  using namespace glass::fs;
  REQUIRE(exists(ad.data_dir));
  REQUIRE(exists(ad.config_dir));
  REQUIRE(exists(ad.cache_dir));
  REQUIRE(exists(ad.documents));
  REQUIRE(exists(ad.desktop));
  REQUIRE(exists(ad.pictures));
  REQUIRE(exists(ad.music));
  REQUIRE(exists(ad.video));
  REQUIRE(exists(ad.download));
  ad.removeAll();
  REQUIRE(!exists(ad.data_dir));
  REQUIRE(!exists(ad.config_dir));
  REQUIRE(!exists(ad.cache_dir));
  REQUIRE(!exists(ad.documents));
  REQUIRE(!exists(ad.desktop));
  REQUIRE(!exists(ad.pictures));
  REQUIRE(!exists(ad.music));
  REQUIRE(!exists(ad.video));
  REQUIRE(!exists(ad.download));
}

TEST_CASE("App directories are all absolute paths", "[Directories]") {
  auto ad = glass::Directories("test2");
  REQUIRE(ad.data_dir.is_absolute());
  REQUIRE(ad.config_dir.is_absolute());
  REQUIRE(ad.cache_dir.is_absolute());
  REQUIRE(ad.documents.is_absolute());
  REQUIRE(ad.desktop.is_absolute());
  REQUIRE(ad.pictures.is_absolute());
  REQUIRE(ad.music.is_absolute());
  REQUIRE(ad.video.is_absolute());
  REQUIRE(ad.download.is_absolute());
  ad.removeAll();
}

TEST_CASE("Can open a test json string") {
  const auto *const test_json = R"({
  "glossary": {
    "title": "example glossary",
    "num": 5,
    "GlossDiv": {
      "title": "S",
      "GlossList": {
        "GlossEntry": {
          "ID": "SGML",
          "SortAs": "SGML",
          "GlossTerm": "Standard Generalized Markup Language",
          "Acronym": "SGML",
          "Abbrev": "ISO 8879:1986",
          "GlossDef": {
            "para": "A meta-markup language, used to create markup languages such as DocBook.",
            "GlossSeeAlso": ["GML", "XML"]
          },
          "GlossSee": "markup"
        }
      }
    }
  }
})";
  auto j = glass::json::parse(test_json);
  REQUIRE(j["glossary"]["title"] == "example glossary");
  REQUIRE(j["glossary"]["num"] == 5);

  auto j2 = glass::json{
      {"pi", 3.141} // NOLINT
  };
}

TEST_CASE("Empty strings turn into empty json objects") {
  std::string str{"{}"};
  REQUIRE(nlohmann::json::parse(str).empty());

  auto test = std::ofstream{"test.json"};
  test << std::setw(4) << nlohmann::json{};
  test.close();

  REQUIRE(nlohmann::json::parse(std::ifstream{"test.json"}).empty());
  glass::fs::remove_all("test.json");

  // TODO: Check back in some day. Seems like a compiler bug with gcc
//  REQUIRE(nlohmann::json{nlohmann::json{}}.empty());
}

TEST_CASE("Can create a json settings file in a folder", "[createSettingsFile]") {
  const auto settings = glass::json{
      {"setting1", 0},
      {"setting2", 1},
      {"setting3", false},
      {"setting4", "test"}
  };
  constexpr auto test_file_name = R"(test_settings.json)";
  auto file_path = glass::fs::current_path() / test_file_name;
  glass::fs::remove_all(file_path); // cleanup
  REQUIRE(!glass::fs::exists(file_path));

  {
    auto res = glass::createSettingsFile(file_path, settings);
    REQUIRE(res.success);
    REQUIRE(res.error == glass::createFile_result::Error::none);
    REQUIRE(glass::fs::exists(file_path));
  }

  glass::fs::remove_all(file_path); //cleanup
  REQUIRE(!glass::fs::exists(file_path));
}

TEST_CASE("createFile_result::Error::invalid_path", "[createSettingsFile]") {
  const auto settings = glass::json{
      {"setting1", 0},
      {"setting2", 1},
      {"setting3", false},
      {"setting4", "test"}
  };
  auto file_path = glass::fs::current_path();
  auto res = glass::createSettingsFile(file_path, settings);
  REQUIRE(!res.success);
  REQUIRE(res.error == glass::createFile_result::Error::invalid_path);
}

TEST_CASE("Can access settings in a file", "[settingsAsJson]") {
  auto settings_path = glass::fs::current_path();
  const auto settings = glass::json{
      {"setting1", 0},
      {"setting2", 1},
      {"setting3", false},
      {"setting4", "test"}
  };
  constexpr auto test_file_name = R"(test_settings.json)";
  auto file_path = glass::fs::current_path() / test_file_name;
  glass::fs::remove_all(file_path); // cleanup
  REQUIRE(!glass::fs::exists(file_path));
  auto res = glass::createSettingsFile(file_path, settings);
  REQUIRE(res.success);

  {
    auto settings_res = glass::settingsAsJson(file_path);
    REQUIRE(settings_res.success);
    REQUIRE(settings_res.error == glass::settings_result::Error::none);
    REQUIRE(settings_res.json_object == settings);
  }

  glass::fs::remove_all(file_path); // cleanup
}

TEST_CASE("settings_result::Error::invalid_path", "[settingsAsJson]") {
  auto file_path = glass::fs::current_path();
  auto settings_res = glass::settingsAsJson(file_path);
  REQUIRE(!settings_res.success);
  REQUIRE(settings_res.error == glass::settings_result::Error::invalid_path);
}

TEST_CASE("settings_result::Error::no_settings_file", "[settingsAsJson]") {
  auto settings_res = glass::settingsAsJson(glass::fs::current_path() / "file_path.json");
  REQUIRE(!settings_res.success);
  REQUIRE(settings_res.error == glass::settings_result::Error::no_settings_file);
}

TEST_CASE("Returns an empty object if file doesn't exist", "[fetchSettings]") {
  auto file_path = glass::fs::path{"test.json"};
  glass::fs::remove_all(file_path);

  {
    auto obj = glass::fetchSettings(file_path);
    REQUIRE(obj.empty());
    REQUIRE(glass::fs::exists(file_path));

    const auto settings = glass::json{
        {"setting1", 0},
        {"setting2", 1},
        {"setting3", false},
        {"setting4", "test"}
    };
    auto res = glass::createSettingsFile(file_path, settings);
    REQUIRE(res.success);
    REQUIRE(res.error == glass::createFile_result::Error::none);
    REQUIRE(glass::fs::exists(file_path));

    obj = glass::fetchSettings(file_path);
    REQUIRE(obj == settings);
  }

  glass::fs::remove_all(file_path);
}