//
// Created by Mike Loomis on 4/21/2020.
//

#include <catch2/catch.hpp>
#include <fstream>
#include <iomanip>

#include "../src/app.hpp"

TEST_CASE("Creates a file if it doesn't exist", "[SettingsCache]") {
  auto file_path = glass::fs::current_path() / "test.json";
  glass::fs::remove_all(file_path);
  REQUIRE(!glass::fs::exists(file_path));
  auto settings = glass::SettingCache{file_path};
  REQUIRE(glass::fs::exists(file_path));
  REQUIRE(settings.settings.empty());

  glass::fs::remove_all(file_path);
}

TEST_CASE("Doesn't create a file if it already exists", "[SettingsCache]") {
  auto file_path = glass::fs::current_path() / "test.json";
  glass::fs::remove_all(file_path);
  REQUIRE(!glass::fs::exists(file_path));

  const auto object = glass::json{
      {"setting1", 0},
      {"setting2", 1},
      {"setting3", false},
      {"setting4", "test"}
  };

  {
    auto file = std::ofstream{file_path};
    file << std::setw(4) << object;
    file.close();
  }

  auto settings = glass::SettingCache{file_path};
  REQUIRE(glass::fs::exists(file_path));
  REQUIRE(settings.settings == object);

  glass::fs::remove_all(file_path);
}

TEST_CASE("Can fetch and commit data to disk", "[SettingsCache]") {
  auto file_path = glass::fs::current_path() / "test.json";
  glass::fs::remove_all(file_path);
  REQUIRE(!glass::fs::exists(file_path));

  auto settings = glass::SettingCache{file_path};

  const auto object = glass::json{
      {"setting1", 0},
      {"setting2", 1},
      {"setting3", false},
      {"setting4", "test"}
  };
  settings.settings = object;
  REQUIRE(glass::fetchSettings(file_path).empty());
  settings.commit();
  REQUIRE(glass::fetchSettings(file_path) == object);
  settings.settings = nlohmann::json{};
  REQUIRE(settings.settings.empty());
  settings.fetch();
  REQUIRE(settings.settings == object);
  glass::fs::remove_all(file_path);
}

TEST_CASE("App object creates config and cache files", "[app]") {
  auto dirs = glass::Directories{"test", glass::fs::current_path()};
  auto app = glass::App{dirs};
  REQUIRE(glass::fs::exists(dirs.config_dir));
  REQUIRE(glass::fs::exists(dirs.cache_dir));
  dirs.removeAll();
  REQUIRE(!glass::fs::exists(dirs.cache_dir));
}

TEST_CASE("App can open projects at a directory", "[app]") {
  auto dirs = glass::Directories{"test_app_dirs", glass::fs::current_path()};
  auto app = glass::App{dirs};
  const auto *const folder_name = "test_project";
  const auto project_root = glass::fs::current_path() / folder_name;
  glass::fs::remove_all(project_root);

  REQUIRE(glass::createFolder(folder_name, glass::fs::current_path()).success);

  app.useFolderAsProject(project_root);
  REQUIRE(glass::fs::exists(project_root / ".glass"));

  // Make sure the project dir is cached for later
  auto cache_settings = glass::fetchSettings(dirs.cache_dir / glass::App::cache_filename);
  REQUIRE(cache_settings["last_project_root"] == project_root.string());

  glass::fs::remove_all(project_root);
  dirs.removeAll();
  REQUIRE(!glass::fs::exists(dirs.cache_dir));
}

TEST_CASE("External facing interface allows for adding settings", "[app]") {
  auto dirs = glass::Directories{"test_app_dirs", glass::fs::current_path()};
  auto app = glass::App{dirs};

  auto cache_settings = glass::fetchSettings(dirs.cache_dir / glass::App::cache_filename);
  auto config_settings = glass::fetchSettings(dirs.config_dir / glass::App::config_filename);
  REQUIRE(cache_settings["test_cache_item"] == nullptr);
  REQUIRE(config_settings["test_config_item"] == nullptr);

  const auto *test_key = "test_item";
  const auto *test_val = "test_value";
  auto settings_to_add = nlohmann::json{
      {test_key, test_val}
  };

  app.addCacheData(settings_to_add);
  app.addConfigs(settings_to_add);

  cache_settings = glass::fetchSettings(dirs.cache_dir / glass::App::cache_filename);
  config_settings = glass::fetchSettings(dirs.config_dir / glass::App::config_filename);
  REQUIRE(cache_settings[test_key] == settings_to_add[test_key]);
  REQUIRE(config_settings[test_key] == settings_to_add[test_key]);

  dirs.removeAll();
  REQUIRE(!glass::fs::exists(dirs.cache_dir));
}

TEST_CASE("Can get settings through app interface", "[app]") {
  auto dirs = glass::Directories{"test_app_dirs", glass::fs::current_path()};
  auto app = glass::App{dirs};

  const auto *test_key = "test_item";
  const auto *test_val = "test_value";
  auto settings_to_add = nlohmann::json{
      {test_key, test_val}
  };

  REQUIRE(!app.getCache<std::string>(test_key).contains);
  REQUIRE(!app.getConfig<std::string>(test_key).contains);

  app.addCacheData(settings_to_add);
  app.addConfigs(settings_to_add);

  REQUIRE(app.getCache<std::string>(test_key).value == test_val);
  REQUIRE(app.getConfig<std::string>(test_key).value == test_val);

  dirs.removeAll();
  REQUIRE(!glass::fs::exists(dirs.cache_dir));
}