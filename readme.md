[![Build status](https://ci.appveyor.com/api/projects/status/g92vaf0g1rhpluic/branch/master?svg=true)](https://ci.appveyor.com/project/mikeloomis/glass/branch/master)

# Build Requirements
These tools must be installed prior to building successfully
- Conan https://conan.io/downloads.html
- Qt5 https://www.qt.io/download-qt-installer
o